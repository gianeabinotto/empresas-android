![N|Solid](logo_ioasys.png)

# README #

### Bibliotecas usadas ###

* Glide: usada para download de imagens
* Retrofit: usada para efetuar requisições com o servidor
* RecyclerView: usada para listar as empresas
* CardView: usada como layout para representar cada uma das empresas na lista


### O que faria se tivesse mais tempo ###

* Faria alguns bônus como utilizar alguma ferramenta de Injeção de Dependência e testes unitários


### Instruções de como executar o projeto ###

* O device deve possuir uma API >= 21
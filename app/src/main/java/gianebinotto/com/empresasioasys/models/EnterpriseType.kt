package gianebinotto.com.empresasioasys.models

data class EnterpriseType(val id: Int, val enterprise_type_name: String?)
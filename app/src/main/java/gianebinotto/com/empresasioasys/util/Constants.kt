package gianebinotto.com.empresasioasys.util

class Constants {

    //constantes usadas na aplicação
    companion object {
        const val URL = "http://empresas.ioasys.com.br"
        const val ACCESSTOKEN = "access-token"
        const val UID = "uid"
        const val CLIENT = "client"
    }
}
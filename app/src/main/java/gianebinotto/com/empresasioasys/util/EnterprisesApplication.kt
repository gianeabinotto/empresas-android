package gianebinotto.com.empresasioasys.util

import android.app.Application

class EnterprisesApplication: Application() {
    override fun onCreate() {
        super.onCreate()

        //inicia o Singleton do SharedPreference
        PreferencesSingleton.instance.setSharedPref(this)
    }
}
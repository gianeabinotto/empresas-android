package gianebinotto.com.empresasioasys.util

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class MyAppGlide: AppGlideModule()
package gianebinotto.com.empresasioasys.util

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

//funções que gerenciam o SharedPreference
class PreferencesSingleton private constructor() {
    enum class Keys(val key: String) {
        ACCESSTOKEN("access-token"),
        CLIENT("client"),
        UID("uid"),
    }

    private object Holder { val prefInstance = PreferencesSingleton() }
    private var sharedPreferences: SharedPreferences? = null

    companion object {
        val instance: PreferencesSingleton by lazy { Holder.prefInstance }
    }

    fun setSharedPref(context: Context){
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    }

    fun getPref(key: Keys): String {
        return instance.sharedPreferences?.getString(key.key, "") ?: ""
    }

    fun savePref(key: Keys, value: String) {
        val editor = instance.sharedPreferences?.edit()

        editor?.putString(key.key, value)
        editor?.apply()
    }
}
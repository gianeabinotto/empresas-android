package gianebinotto.com.empresasioasys.api

import com.google.gson.JsonObject
import gianebinotto.com.empresasioasys.util.Constants
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

//interface que define os métodos de requisição ao servidor bem como a função de criação do objeto Retrofit
interface ApiInterface {
    @POST("/api/v1/users/auth/sign_in")
    fun login(@Body user: JsonObject) : Call<Void>

    @GET("/api/v1/enterprises")
    fun searchEnterprisesByName(@Query("name") text: String, @Header(Constants.CLIENT) client: String, @Header(Constants.ACCESSTOKEN) accessToken: String, @Header(Constants.UID) uid: String): Call<JsonObject>

    @GET("/api/v1/enterprises/{id}")
    fun getEnterprise(@Path("id") id: Int, @Header(Constants.CLIENT) client: String, @Header(Constants.ACCESSTOKEN) accessToken: String, @Header(Constants.UID) uid: String): Call<JsonObject>

    companion object Factory {
        fun create(): ApiInterface {
            val retrofit = retrofit2.Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(Constants.URL)
                    .build()

            return retrofit.create(ApiInterface::class.java)
        }
    }
}
package gianebinotto.com.empresasioasys.modulos.main

class MainContract {
    interface View {
        fun startLogin()
        fun startListagemEmpresa()
        fun finishActivity()
    }

    interface Presenter {
        fun onDestroy()
        fun attach(view: MainContract.View)
        fun getCurrentScreen()
    }
}
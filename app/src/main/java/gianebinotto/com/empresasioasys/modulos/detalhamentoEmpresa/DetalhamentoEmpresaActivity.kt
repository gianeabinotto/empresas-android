package gianebinotto.com.empresasioasys.modulos.detalhamentoEmpresa

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import gianebinotto.com.empresasioasys.R
import kotlinx.android.synthetic.main.activity_detalhamento_empresa.*

class DetalhamentoEmpresaActivity : AppCompatActivity(), DetalhamentoEmpresaContract.View {
    lateinit var presenter: DetalhamentoEmpresaContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalhamento_empresa)

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.title = ""

        presenter = DetalhamentoEmpresaPresenter()
        presenter.attach(this)
        presenter.buscarEmpresa(intent.getIntExtra("id", 0))
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun setToolbarTitle(title: String){
        supportActionBar?.title = title
    }

    override fun getImageView(): ImageView {
        return iv_empresa
    }

    override fun getActivity(): DetalhamentoEmpresaActivity {
        return this
    }

    override fun setDescription(desc: String) {
        tv_desc_empresa.text = desc
    }

    override fun falha() {
        Toast.makeText(this, getString(R.string.falha_buscar_empresa), Toast.LENGTH_SHORT).show()
    }

    override fun finishActivity() {
        finish()
    }

    override fun initProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun finishProgressBar() {
        progressBar.visibility = View.GONE
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }
}

package gianebinotto.com.empresasioasys.modulos.login

class LoginContract {
    interface View {
        fun loginNaoAutorizado()
        fun falha()
        fun camposVazios()
        fun emailInvalido()
        fun startListagemEmpresa()
        fun initProgressBar()
        fun finishProgressBar()
        fun finishActivity()
    }

    interface Presenter {
        fun login(email: String, senha: String)
        fun attach(view: LoginContract.View)
        fun onDestroy()
    }
}
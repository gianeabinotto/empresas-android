package gianebinotto.com.empresasioasys.modulos.detalhamentoEmpresa

import android.widget.ImageView

class DetalhamentoEmpresaContract {
    interface View {
        fun setToolbarTitle(title: String)
        fun setDescription(desc: String)
        fun getImageView(): ImageView
        fun getActivity() : DetalhamentoEmpresaActivity
        fun falha()
        fun finishActivity()
        fun initProgressBar()
        fun finishProgressBar()
    }

    interface Presenter {
        fun buscarEmpresa(id: Int)
        fun attach(view: DetalhamentoEmpresaContract.View)
        fun onDestroy()
    }
}
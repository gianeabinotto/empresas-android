package gianebinotto.com.empresasioasys.modulos.listagemEmpresa

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.load.engine.DiskCacheStrategy
import gianebinotto.com.empresasioasys.R
import gianebinotto.com.empresasioasys.models.Enterprise
import gianebinotto.com.empresasioasys.util.Constants
import gianebinotto.com.empresasioasys.util.GlideApp
import kotlinx.android.synthetic.main.cardview_empresa.view.*

class ListagemEmpresaAdapter(private val enterprises: ArrayList<Enterprise>?, private val listagemEmpresaActivity: ListagemEmpresaActivity, private val clickListener: OnClickListener): RecyclerView.Adapter<ListagemEmpresaAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val createdView = LayoutInflater.from(listagemEmpresaActivity).inflate(R.layout.cardview_empresa, parent, false)
        val view = ViewHolder(createdView, listagemEmpresaActivity)
        return view
    }

    override fun getItemCount(): Int {
        return enterprises?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, posicao: Int) {
        enterprises.let {
            holder.bind(it!![posicao], clickListener)
        }
    }

    interface OnClickListener {
        fun onClick(position: Int)
    }

    class ViewHolder(itemView: View, private var activity: ListagemEmpresaActivity) : RecyclerView.ViewHolder(itemView) {
        private val imagem = itemView.iv_cardview
        private val nomeEmpresa = itemView.tv_nome_empresa
        private val tipoEmpresa = itemView.tv_tipo_empresa
        private val pais = itemView.tv_pais

        fun bind(empresa: Enterprise, clickListener: OnClickListener) {
            nomeEmpresa.text = empresa.enterprise_name
            tipoEmpresa.text = empresa.enterprise_type.enterprise_type_name
            pais.text = empresa.country

            if (!empresa.photo.isNullOrEmpty()) {
                val url: String = Constants.URL + empresa.photo
                GlideApp.with(activity).load(url).diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).into(imagem)
            }

            itemView.setOnClickListener {
                clickListener.onClick(adapterPosition)
            }
        }
    }
}
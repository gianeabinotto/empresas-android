package gianebinotto.com.empresasioasys.modulos.login

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import gianebinotto.com.empresasioasys.R
import gianebinotto.com.empresasioasys.modulos.listagemEmpresa.ListagemEmpresaActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity: AppCompatActivity(), LoginContract.View {
    lateinit var presenter: LoginContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        presenter = LoginPresenter()
        presenter.attach(this)
    }

    fun login(v: View){
        presenter.login(et_email.text.toString(), et_senha.text.toString())
    }

    override fun initProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun finishProgressBar() {
        progressBar.visibility = View.GONE
    }

    override fun finishActivity() {
        finish()
    }

    override fun camposVazios() {
        Toast.makeText(this, getString(R.string.campos_vazios), Toast.LENGTH_SHORT).show()
    }

    override fun emailInvalido() {
        Toast.makeText(this, getString(R.string.email_invalido), Toast.LENGTH_SHORT).show()
    }

    override fun loginNaoAutorizado() {
        Toast.makeText(this, getString(R.string.email_senha_invalidas), Toast.LENGTH_SHORT).show()
    }

    override fun falha() {
        Toast.makeText(this, getString(R.string.falha_login), Toast.LENGTH_SHORT).show()
    }

    override fun startListagemEmpresa() {
        startActivity(Intent(this, ListagemEmpresaActivity::class.java))
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }
}

package gianebinotto.com.empresasioasys.modulos.login

import android.util.Patterns
import com.google.gson.JsonObject
import gianebinotto.com.empresasioasys.api.ApiInterface
import gianebinotto.com.empresasioasys.util.Constants
import gianebinotto.com.empresasioasys.util.PreferencesSingleton
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginPresenter: LoginContract.Presenter {
    private var view: LoginContract.View? = null

    //verifica se as informações inseridas pelo usuário são válidas e envia a requisição de login ao servidor
    override fun login(email: String, senha: String) {
        if(email.isEmpty() || senha.isEmpty()){
            view?.camposVazios()
            return
        }

        if(emailInvalido(email)){
            view?.emailInvalido()
            return
        }

        view?.initProgressBar()
        val call = ApiInterface.create().login(getJsonObject(email, senha))
        call.enqueue(object: Callback<Void> {
            override fun onResponse(call: Call<Void?>?, response: Response<Void?>?) {
                val accessToken = response?.headers()?.get(Constants.ACCESSTOKEN)
                val uid = response?.headers()?.get(Constants.UID)
                val client = response?.headers()?.get(Constants.CLIENT)

                view?.finishProgressBar()

                if(!accessToken.isNullOrEmpty() && !uid.isNullOrEmpty() && !client.isNullOrEmpty()) {
                    saveAuth(accessToken!!, uid!!, client!!)
                    view?.startListagemEmpresa()
                    view?.finishActivity()
                } else
                    view?.loginNaoAutorizado()
            }

            override fun onFailure(call: Call<Void?>?, t: Throwable?) {
                view?.finishProgressBar()
                view?.falha()
            }
        })
    }

    //constroi o objeto json com email e senha para ser enviado como body da requisição
    fun getJsonObject(email: String, senha: String) : JsonObject{
        val json = JsonObject()
        json.addProperty("email", email)
        json.addProperty("password", senha)
        return json
    }

    //salva as chaves de autorização no SharedPreferences
    fun saveAuth(accessToken: String, uid: String, client: String){
        PreferencesSingleton.instance.savePref(PreferencesSingleton.Keys.ACCESSTOKEN, accessToken)
        PreferencesSingleton.instance.savePref(PreferencesSingleton.Keys.UID, uid)
        PreferencesSingleton.instance.savePref(PreferencesSingleton.Keys.CLIENT, client)
    }

    fun emailInvalido(email: String): Boolean {
        return !Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    override fun attach(view: LoginContract.View) {
        this.view = view
    }

    override fun onDestroy() {
        view = null
    }
}
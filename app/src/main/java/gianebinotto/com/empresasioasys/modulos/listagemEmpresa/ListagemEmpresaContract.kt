package gianebinotto.com.empresasioasys.modulos.listagemEmpresa

class ListagemEmpresaContract {
    interface View {
        fun setAdapter(adapter: ListagemEmpresaAdapter?)
        fun getActivity() : ListagemEmpresaActivity
        fun startDetalhamentoEmpresa(id: Int)
        fun initProgressBar()
        fun finishProgressBar()
        fun falha()
    }

    interface Presenter {
        fun buscarEmpresas(text: String)
        fun attach(view: ListagemEmpresaContract.View)
        fun onDestroy()
    }
}
package gianebinotto.com.empresasioasys.modulos.main

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import gianebinotto.com.empresasioasys.modulos.listagemEmpresa.ListagemEmpresaActivity
import gianebinotto.com.empresasioasys.modulos.login.LoginActivity

class MainActivity : AppCompatActivity(), MainContract.View {
    lateinit var presenter: MainContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        presenter = MainPresenter()
        presenter.attach(this)
        presenter.getCurrentScreen()
    }

    override fun startListagemEmpresa() {
        startActivity(Intent(this, ListagemEmpresaActivity::class.java))
    }

    override fun startLogin() {
        startActivity(Intent(this, LoginActivity::class.java))
    }

    override fun finishActivity() {
        finish()
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }
}

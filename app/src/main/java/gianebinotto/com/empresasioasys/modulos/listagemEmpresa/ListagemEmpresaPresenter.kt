package gianebinotto.com.empresasioasys.modulos.listagemEmpresa

import com.google.gson.Gson
import com.google.gson.JsonObject
import gianebinotto.com.empresasioasys.api.ApiInterface
import gianebinotto.com.empresasioasys.models.Enterprise
import gianebinotto.com.empresasioasys.util.PreferencesSingleton
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ListagemEmpresaPresenter: ListagemEmpresaContract.Presenter {
    private var view: ListagemEmpresaContract.View? = null
    var adapter: ListagemEmpresaAdapter? = null
    var empresas: ArrayList<Enterprise> = ArrayList()

    //busca as empresas de acordo com o que o usuário inseriu na SearchView
    override fun buscarEmpresas(text: String) {
        empresas.clear()
        view?.initProgressBar()

        val call = ApiInterface.create().searchEnterprisesByName(text, PreferencesSingleton.instance.getPref(PreferencesSingleton.Keys.CLIENT),
                PreferencesSingleton.instance.getPref(PreferencesSingleton.Keys.ACCESSTOKEN), PreferencesSingleton.instance.getPref(PreferencesSingleton.Keys.UID))
        call.enqueue(object: Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject?>?, response: Response<JsonObject?>?) {
                response?.body()?.let {
                    if (it.has("enterprises")) {
                        empresas.addAll(Gson().fromJson(it.get("enterprises"), Array<Enterprise>::class.java))
                        initAdapter()?.notifyDataSetChanged()
                    }
                }
                view?.finishProgressBar()
            }

            override fun onFailure(call: Call<JsonObject?>?, t: Throwable?) {
                view?.finishProgressBar()
                view?.falha()
            }
        })
    }

    //inicia o adapter para a RecyclerView caso ele não esteja inicializado
    fun initAdapter(): ListagemEmpresaAdapter? {
        if(adapter == null) {
            val clickListener: ListagemEmpresaAdapter.OnClickListener = object : ListagemEmpresaAdapter.OnClickListener{
                override fun onClick(position: Int) {
                    view?.startDetalhamentoEmpresa(empresas.get(position).id)
                }
            }
            adapter = ListagemEmpresaAdapter(empresas, view!!.getActivity(), clickListener)
            view?.setAdapter(adapter)
        }
        return adapter
    }

    override fun attach(view: ListagemEmpresaContract.View) {
        this.view = view
    }

    override fun onDestroy() {
        view = null
    }
}
package gianebinotto.com.empresasioasys.modulos.listagemEmpresa

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SearchView
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.View
import android.view.ViewStub
import android.widget.Toast
import gianebinotto.com.empresasioasys.R
import gianebinotto.com.empresasioasys.modulos.detalhamentoEmpresa.DetalhamentoEmpresaActivity
import kotlinx.android.synthetic.main.activity_listagem_empresa.*

class ListagemEmpresaActivity : AppCompatActivity(), ListagemEmpresaContract.View {
    lateinit var presenter: ListagemEmpresaContract.Presenter
    lateinit var viewstub: ViewStub

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listagem_empresa)

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        viewstub = findViewById(R.id.stub)
        viewstub.inflate()

        presenter = ListagemEmpresaPresenter()
        presenter.attach(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)

        val searchView = menu?.findItem(R.id.action_search)?.actionView as SearchView
        searchView.onActionViewCollapsed()

        return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val searchItem = menu?.findItem(R.id.action_search)
        val searchView = searchItem?.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                presenter.buscarEmpresas(query)
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                viewstub.visibility = View.GONE
                return false
            }
        })

        return super.onPrepareOptionsMenu(menu)
    }

    override fun setAdapter(adapter: ListagemEmpresaAdapter?) {
        rv_empresas.adapter = adapter
    }

    override fun getActivity(): ListagemEmpresaActivity {
        return this
    }

    override fun startDetalhamentoEmpresa(id: Int) {
        startActivity(Intent(this, DetalhamentoEmpresaActivity::class.java).putExtra("id", id))
    }

    override fun initProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    override fun finishProgressBar() {
        progressBar.visibility = View.GONE
    }

    override fun falha() {
        Toast.makeText(this, getString(R.string.falha_buscar_empresas), Toast.LENGTH_SHORT).show();
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }
}
package gianebinotto.com.empresasioasys.modulos.detalhamentoEmpresa

import com.google.gson.Gson
import com.google.gson.JsonObject
import gianebinotto.com.empresasioasys.api.ApiInterface
import gianebinotto.com.empresasioasys.models.Enterprise
import gianebinotto.com.empresasioasys.util.Constants
import gianebinotto.com.empresasioasys.util.GlideApp
import gianebinotto.com.empresasioasys.util.PreferencesSingleton
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetalhamentoEmpresaPresenter: DetalhamentoEmpresaContract.Presenter {
    private var view: DetalhamentoEmpresaContract.View? = null

    //busca a empresa que o usuário clicou através do id
    override fun buscarEmpresa(id: Int) {
        view?.initProgressBar()

        val call = ApiInterface.create().getEnterprise(id, PreferencesSingleton.instance.getPref(PreferencesSingleton.Keys.CLIENT),
                PreferencesSingleton.instance.getPref(PreferencesSingleton.Keys.ACCESSTOKEN), PreferencesSingleton.instance.getPref(PreferencesSingleton.Keys.UID))
        call.enqueue(object: Callback<JsonObject> {
            override fun onResponse(call: Call<JsonObject?>?, response: Response<JsonObject?>?) {
                response?.body()?.let {
                    if (it.has("enterprise")) {
                        val empresa = Gson().fromJson(it.get("enterprise"), Enterprise::class.java)
                        empresa.let {
                            view?.setDescription(empresa.description ?: "")
                            view?.setToolbarTitle(empresa.enterprise_name ?: "")

                            if (!empresa.photo.isNullOrEmpty() && view != null) {
                                val url: String = Constants.URL + empresa.photo
                                GlideApp.with(view!!.getActivity()).load(url).into(view!!.getImageView())
                            }
                        }
                    }
                }
                view?.finishProgressBar()
            }

            override fun onFailure(call: Call<JsonObject?>?, t: Throwable?) {
                view?.finishProgressBar()
                view?.falha()
            }
        })
    }

    override fun attach(view: DetalhamentoEmpresaContract.View) {
        this.view = view
    }

    override fun onDestroy() {
        view = null
    }
}
package gianebinotto.com.empresasioasys.modulos.main

import gianebinotto.com.empresasioasys.util.PreferencesSingleton

class MainPresenter: MainContract.Presenter {
    private var view: MainContract.View? = null

    override fun getCurrentScreen() {
        val accessToken = PreferencesSingleton.instance.getPref(PreferencesSingleton.Keys.ACCESSTOKEN)
        if(accessToken.isEmpty())
            view?.startLogin()
        else
            view?.startListagemEmpresa()
        view?.finishActivity()
    }

    override fun attach(view: MainContract.View) {
        this.view = view
    }

    override fun onDestroy() {
        view = null
    }
}